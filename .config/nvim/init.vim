set previewheight=10
"set rtp+=/usr/local/Homebrew/opt/fzf "mac

call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-projectionist'
Plug 'puremourning/vimspector'
Plug 'nlknguyen/papercolor-theme'
Plug 'tpope/vim-vinegar'
Plug 'pangloss/vim-javascript'
Plug 'tpope/vim-surround'
Plug 'retorillo/istanbul.vim'
Plug 'plasticboy/vim-markdown'
Plug 'leafgarland/typescript-vim'
" For func argument completion
"Plug '/usr/local/opt/fzf' "mac
Plug '~/.fzf' "windows
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-tsserver', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-eslint', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-git', {'do': 'yarn install --frozen-lockfile'}
Plug 'khanghoang/coc-jest', {'branch': 'v0.0.3', 'do': 'yarn install --frozen-lockfile'}
Plug 'fannheyward/coc-docthis', {'do': 'yarn install --frozen-lockfile'}
Plug 'josa42/coc-docker', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-html', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-css', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-lists', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-snippets', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-yaml', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-java', {'do': 'yarn install --frozen-lockfile'}
Plug 'dsummersl/gundo.vim'
Plug 'janko/vim-test'
Plug 'tpope/vim-dispatch'
call plug#end()

syntax enable
filetype indent on
colorscheme PaperColor

" key mappings ------------------ {{{
let mapleader=","

nnoremap <leader>b :call vimspector#ToggleBreakpoint()<CR>
nnoremap <Backspace> <C-o>
nnoremap <Delete> <C-i>
nnoremap <silent> <Leader>gs :Gstatus<CR>:10wincmd_<CR>
nnoremap <leader>c :pclose<CR>
tnoremap <A-h> <C-\><C-N><C-w>h
tnoremap <A-j> <C-\><C-N><C-w>j
tnoremap <A-k> <C-\><C-N><C-w>k
tnoremap <A-l> <C-\><C-N><C-w>l
inoremap <A-h> <C-\><C-N><C-w>h
inoremap <A-j> <C-\><C-N><C-w>j
inoremap <A-k> <C-\><C-N><C-w>k
inoremap <A-l> <C-\><C-N><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l
tnoremap <Esc> <C-\><C-n>
nnoremap <C-p> :FZF<CR>
nnoremap <leader>t :split<CR><C-w>j:resize 10<CR>:term<CR>i
nnoremap k gk
nnoremap j gj
nnoremap <leader><space> :nohlsearch<CR>
nnoremap <leader>- :split<CR>:wincmd j<CR>
nnoremap <leader>\ :vsplit<CR>:wincmd l<CR>
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>H :wincmd H<CR>
nnoremap <leader>L :wincmd L<CR>
nnoremap <leader>J :wincmd J<CR>
nnoremap <leader>K :wincmd K<CR>
nnoremap <leader>g :Gdiff<CR>
nnoremap <leader>d :windo diffthis<CR>
nnoremap <leader>D :windo diffoff<CR>
nnoremap <leader>e :Errors<CR>
nnoremap <leader>E :lclose<CR>
nnoremap <TAB> :tabn<CR>
nnoremap <A-TAB> :tabn<CR>
nnoremap <S-TAB> :tabp<CR>
noremap <leader>y "+y:let @*=@+<CR>
map <leader>p "+p

nmap <leader>is <F10>
nmap <leader>ii <F11>
nmap <leader>io <F12>
nmap <leader>iS <F3>
nmap <leader>ic <F5>
nmap <leader>ir <F4>
nmap <leader>iR :VimspectorReset<cr>
nmap <leader>ip <F6>
nmap <leader>ib <F9>
nmap <leader>iB ,<F9>
" }}}

let g:neosnippet#enable_completed_snippet = 1

" set commands ------------------- {{{
set colorcolumn=80
set laststatus=2
set autoread
set backspace=indent,eol,start
set mouse=a
set spell spelllang=en_us
set nospell
set ignorecase
set smartcase
set cursorline
set foldmethod=manual
set sessionoptions-=options
set directory=~/.vim/backups//
set backupdir=~/.vim/backups//
set undofile
set undodir=~/.vim/undo//
set undolevels=1000
set undoreload=10000
set tabstop=2
set softtabstop=0
set expandtab
set shiftwidth=2
set smarttab
set number
set showcmd
set wildmenu
set lazyredraw
set showmatch
set hlsearch
set background=dark
set hidden
set completeopt=menuone,longest,preview
set grepprg=rg\ --vimgrep\ --no-heading\ --smart-case

highlight Pmenu guibg=#015E86 gui=bold
let g:vimspector_enable_mappings = 'HUMAN'

sign define marker text=* texthl=Search linehl=Search
" }}}

function! Status()
	set statusline=
	set statusline+=%#PmenuSel#
	set statusline+=%{FugitiveStatusline()}
	set statusline+=%#LineNr#
	set statusline+=\ %{expand('%:h')}/%#CursorColumn#%t%#LineNr#
	set statusline+=%m
	set statusline+=%=
	set statusline+=%#CursorColumn#
	set statusline+=\ %y
	set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
	set statusline+=\[%{&fileformat}\]
	set statusline+=\ %p%%
	set statusline+=\ %l:%c
endfunction
call Status()

highlight StorageClass cterm=italic
highlight Statement cterm=italic
highlight Constant cterm=italic
highlight Type cterm=italic
highlight Boolean cterm=italic
highlight Conditional cterm=italic
highlight Operator cterm=italic
highlight htmlArg cterm=italic
highlight htmlTag cterm=italic
highlight comment cterm=italic

command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --smart-case --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>), 1, <bang>0)























" Better display for messages
set cmdheight=2

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` to navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> <leader>lg <Plug>(coc-definition)
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Use <tab> for select selections ranges, needs server support, like: coc-tsserver, coc-python
"nmap <silent> <TAB> <Plug>(coc-range-select)
"xmap <silent> <TAB> <Plug>(coc-range-select)
"xmap <silent> <S-TAB> <Plug>(coc-range-select-backword)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

nnoremap <silent> <space>l  :<C-u>CocList<CR>



" Using CocList
" Show all diagnostics
nnoremap <silent> <leader>ca  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <leader>ce  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <leader>cc  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <leader>co  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <leader>cs  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <leader>cj  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <leader>ck  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <leader>cp  :<C-u>CocListResume<CR>
nnoremap <silent> <leader>cl  :<C-u>CocList<CR>


