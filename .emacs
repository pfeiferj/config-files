(require 'package)

(menu-bar-mode -1)
(tool-bar-mode -1)
(global-linum-mode t)

;;; package managers

(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
(setq package-enable-at-startup nil)
(package-initialize)

(load-theme 'wombat)

;;; use package config

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

;;; evil config

(use-package evil
  :ensure t
  :config
  (evil-mode 1)

  (use-package evil-leader
    :ensure t
    :config
    (global-evil-leader-mode))

  (use-package evil-surround
    :ensure t
    :config
    (global-evil-surround-mode))

  (use-package evil-indent-textobject
    :ensure t))

(dolist (mode '(ag-mode
		flycheck-error-list-mode
		git-rebase-mode))
  (add-to-list 'evil-emacs-state-modes mode))

(evil-add-hjkl-bindings occur-mode-map 'emacs
  (kbd "/")       'evil-search-forward
  (kbd "n")       'evil-search-next
  (kbd "N")       'evil-search-previous
  (kbd "C-d")     'evil-scroll-down
  (kbd "C-u")     'evil-scroll-up
  (kbd "C-w C-w") 'other-window)

(evil-leader/set-leader ",")
(evil-leader/set-key
  "b" 'helm-mini
  "d" 'kill-this-buffer
  "x" 'helm-M-x
  "g" 'evil-goto-definition
  "u" (kbd ":undolist RET")
  "d" (kbd "C-x k")
  "m" 'magit-blame-toggle
  "p" 'helm-projectile
  "j" 'windmove-down
  "k" 'windmove-up
  "l" 'windmove-right
  "h" 'windmove-left)

(defun magit-blame-toggle ()
  "Toggle magit-blame-mode on and off interactively."
  (interactive)
  (if (and (boundp 'magit-blame-mode) magit-blame-mode)
      (magit-blame-quit)
    (call-interactively 'magit-blame)))

(dolist (mode '(twittering-edit-mode
		magit-log-edit-mode))
  (add-to-list 'evil-insert-state-modes mode))

(evil-define-key 'normal global-map (kbd "-")       'helm-find-files)
(evil-define-key 'normal global-map (kbd "<down>")  'evil-next-visual-line)
(evil-define-key 'normal global-map (kbd "<up>")    'evil-previous-visual-line)


;; Make escape quit everything, whenever possible.
(define-key evil-normal-state-map [escape] 'keyboard-escape-quit)
(define-key evil-visual-state-map [escape] 'keyboard-quit)
(define-key minibuffer-local-map [escape] 'keyboard-quit)
(define-key minibuffer-local-ns-map [escape] 'keyboard-quit)
(define-key minibuffer-local-completion-map [escape] 'keyboard-quit)
(define-key minibuffer-local-must-match-map [escape] 'keyboard-quit)
(define-key minibuffer-local-isearch-map [escape] 'keyboard-quit)

;;; helm
(use-package helm
  :ensure t
  :diminish helm-mode
  :commands helm-mode
  :config
  (helm-mode 1)
  (setq helm-buffers-fuzzy-matching t)
  (setq helm-autoresize-mode t)
  (setq helm-buffer-max-length 40))

(use-package helm-projectile
  :commands (helm-projectile helm-projectile-switch-project)
  :ensure t)

;;; twitter
(use-package twittering-mode
  :ensure t
  :commands (twit twittering-mode)
  :config
  (add-hook 'twittering-mode-hook
	    (lambda ()
	      (define-key twittering-mode-map (kbd ",o") 'delete-other-windows)
	      (define-key twittering-mode-map (kbd ",b") 'helm-mini))))

;;; powerline
(use-package powerline
  :ensure t
  :config)
(use-package powerline-evil
  :ensure t)

;;; flycheck
(use-package flycheck
  :ensure t
  :commands flycheck-mode)


;;; company
(use-package company
  :ensure t
  :defer t
  :init
  (global-company-mode)
  :config
					;(setq company-tooltip-common-selection ((t (:inherit company-tooltip-selection :background "yellow2" :foreground "#c82829"))))
					;(setq company-tooltip-selection ((t (:background "yellow2"))))
  (setq company-idle-delay 0.2)
  (setq company-selection-wrap-around t)
  (define-key company-active-map [tab] 'company-complete)
  (define-key company-active-map (kbd "C-n") 'company-select-next)
  (define-key company-active-map (kbd "C-p") 'company-select-previous))

;;; dictionary
(use-package dictionary :ensure t)

;;; projectile
(use-package projectile
  :ensure t
  :defer 1
  :config
  (projectile-global-mode)
  (setq projectile-enable-caching t))

;;; magit
(use-package magit
  :ensure t
  :defer t
  :config
  (setq magit-branch-arguments nil)
  (setq magit-push-always-verify nil)
  (setq magit-last-seen-setup-instructions "1.4.0")
  (magit-define-popup-switch 'magit-log-popup ?f "first parent" "--first-parent"))
(use-package evil-magit
  :ensure t)

;;; undo tree
(use-package undo-tree
  :ensure t
  :diminish t
  :config
  (setq undo-tree-auto-save-history t)
  (setq undo-tree-history-directory-alist
	(list (cons "." (expand-file-name "undo-tree-history" user-emacs-directory)))))

;;; web-beautify
(use-package web-beautify
  :ensure t)

(eval-after-load 'js2-mode
  '(add-hook 'js2-mode-hook
	     (lambda ()
	       (add-hook 'before-save-hook 'web-beautify-js-buffer t t))))

;; Or if you're using 'js-mode' (a.k.a 'javascript-mode')
(eval-after-load 'js
  '(add-hook 'js-mode-hook
	     (lambda ()
	       (add-hook 'before-save-hook 'web-beautify-js-buffer t t))))

(eval-after-load 'json-mode
  '(add-hook 'json-mode-hook
	     (lambda ()
	       (add-hook 'before-save-hook 'web-beautify-js-buffer t t))))

(eval-after-load 'sgml-mode
  '(add-hook 'html-mode-hook
	     (lambda ()
	       (add-hook 'before-save-hook 'web-beautify-html-buffer t t))))

(eval-after-load 'web-mode
  '(add-hook 'web-mode-hook
	     (lambda ()
	       (add-hook 'before-save-hook 'web-beautify-html-buffer t t))))

(eval-after-load 'css-mode
  '(add-hook 'css-mode-hook
	     (lambda ()
	       (add-hook 'before-save-hook 'web-beautify-css-buffer t t))))


;;; diminish
(use-package diminish
  :ensure t)

;;; smooth scrolling
(use-package smooth-scrolling
  :ensure t)

;;; org mode
(use-package org
  :ensure t)
(use-package org-evil
  :ensure t)

;;;;webkit
;(use-package epc
;:ensure t)
;;(let ((default-directory  "~/.emacs.d/lisp/"))
;;  (normal-top-level-add-subdirs-to-load-path))
;(load-file "~/.emacs.d/lisp/webkit.el")
