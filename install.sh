#!/bin/bash
if command -v pamac &> /dev/null
then
    pamac install chromium virt-manager neovim net-tools lutris ferdi nautilus
fi

ssh-keygen -t rsa -C "jacob@pfeifer.dev"

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

git clone https://github.com/lukechilds/zsh-nvm.git ~/.zsh-nvm
cp ./.zshrc ~/

touch ~/.system.zshrc

SCRIPT="$PWD/$0"
SCRIPTPATH=`dirname $SCRIPT`

#neovim
mkdir -p ~/.config/nvim
rm -f ~/.config/nvim/init.vim
rm -f ~/.vimrc
ln -s $SCRIPTPATH/.config/nvim/init.vim ~/.config/nvim/init.vim
ln -s $SCRIPTPATH/.config/nvim/init.vim ~/.vimrc
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

#node
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
nvm install 10

npm install -g yarn

#Operator Mono
echo "Add sshkey to git or download fonts manually. Press any key to continue"
while [ true ] ; do
read -t 3 -n 1
if [ $? = 0 ] ; then
exit ;
else
echo "waiting for the keypress"
fi
done

[ -d "./.local/share/fonts/operator-mono" ] || git submodule update --init --recursive

mkdir -p ~/.local/share/fonts/OpenType
rm -rf ./.local/share/fonts/HCo_OperatorMono
rm ~/.local/share/fonts/OpenType/*
unzip ./.local/share/fonts/operator-mono/HCo_OperatorMono.zip -d ./.local/share/fonts/
ln -s $SCRIPTPATH/.local/share/fonts/HCo_OperatorMono/OpenType/* ~/.local/share/fonts/OpenType/
